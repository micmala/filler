/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler_structs.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 15:58:03 by mmalanch          #+#    #+#             */
/*   Updated: 2018/09/27 15:58:04 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_STRUCTS_H
# define FILLER_STRUCTS_H

typedef struct	s_point
{
	int x;
	int y;
}				t_point;

typedef struct	s_grid
{
	char	**data;
	int		width;
	int		height;
	int		area;
}				t_grid;

typedef struct	s_cut_diff
{
	int top;
	int bottom;
	int left;
	int right;

}				t_cut_diff;

typedef struct	s_filler
{
	char		me;
	char		opp;
	t_grid		map;
	t_grid		init_tkn;
	t_grid		cut_tkn;
	t_cut_diff	cut_diff;
}				t_filler;

#endif
