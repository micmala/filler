/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cut_fucking_token.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 18:32:43 by mmalanch          #+#    #+#             */
/*   Updated: 2018/09/26 18:32:44 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <filler_core.h>

static t_cut_diff	init_cut_diff(void)
{
	t_cut_diff cut_diff;

	cut_diff.right = 0;
	cut_diff.left = 0;
	cut_diff.top = 0;
	cut_diff.bottom = 0;
	return (cut_diff);
}

void				calc_empty_raws(t_filler *handler)
{
	int i;

	while (handler->cut_diff.top < handler->init_tkn.height &&
			!ft_strchr(handler->init_tkn.data[handler->cut_diff.top], '*'))
		handler->cut_diff.top++;
	i = handler->init_tkn.height;
	while (--i >= 0 && !ft_strchr(handler->init_tkn.data[i], '*'))
		handler->cut_diff.bottom++;
}

void				calc_empty_columns(t_filler *flr, bool from_left)
{
	t_point iter;

	iter.x = from_left ? -1 : flr->init_tkn.width;
	if (from_left)
		while ((++iter.x < flr->init_tkn.width))
		{
			iter.y = -1;
			while (++iter.y < flr->init_tkn.height)
			{
				if (flr->init_tkn.data[iter.y][iter.x] == '*')
					return ;
				flr->cut_diff.left += (iter.y + 1 == flr->init_tkn.height);
			}
		}
	else
		while (--iter.x >= 0)
		{
			iter.y = -1;
			while (++iter.y < flr->init_tkn.height)
			{
				if (flr->init_tkn.data[iter.y][iter.x] == '*')
					return ;
				flr->cut_diff.right += (iter.y + 1 == flr->init_tkn.height);
			}
		}
}

void				cut_token(t_filler *h)
{
	int		y;

	h->cut_diff = init_cut_diff();
	calc_empty_raws(h);
	calc_empty_columns(h, true);
	calc_empty_columns(h, false);
	h->cut_tkn.height =
			h->init_tkn.height - h->cut_diff.top - h->cut_diff.bottom;
	h->cut_tkn.width = h->init_tkn.width - h->cut_diff.left - h->cut_diff.right;
	h->cut_tkn.area = h->cut_tkn.width * h->cut_tkn.height;
	h->cut_tkn.data = ft_memalloc(h->cut_tkn.height * sizeof(char *));
	y = 0;
	while (y < h->cut_tkn.height)
	{
		h->cut_tkn.data[y] = ft_strsub(h->init_tkn.data[y + h->cut_diff.top],
				(unsigned int)h->cut_diff.left, (size_t)h->cut_tkn.width);
		y++;
	}
}
