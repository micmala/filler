/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_filler_input.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 18:32:14 by mmalanch          #+#    #+#             */
/*   Updated: 2018/09/26 18:32:15 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <filler_core.h>

static void	get_players(t_filler *filler)
{
	char *input;

	get_next_line(STDIN_FILENO, &input);
	if (ft_strchr(input, '1'))
	{
		filler->me = 'O';
		filler->opp = 'X';
	}
	else
	{
		filler->me = 'X';
		filler->opp = 'O';
	}
	ft_strdel(&input);
}

static void	get_map(t_filler *handler)
{
	char	*line;
	int		i;

	get_next_line(STDIN_FILENO, &line);
	handler->map.height = ft_atoi(&line[8]);
	handler->map.width = ft_atoi(&line[11]);
	ft_strdel(&line);
	handler->map.area = handler->map.height * handler->map.width;
	handler->map.data = (char **)ft_memalloc(handler->map.height
												* sizeof(char *));
	get_next_line(STDIN_FILENO, &line);
	ft_strdel(&line);
	i = 0;
	while (i < handler->map.height)
	{
		get_next_line(STDIN_FILENO, &line);
		handler->map.data[i] = ft_strdup(line + 4);
		ft_strdel(&line);
		i++;
	}
}

static void	get_token(t_filler *handler)
{
	char	*line;
	int		i;
	char	*temp;

	get_next_line(STDIN_FILENO, &line);
	handler->init_tkn.height = ft_atoi(&line[6]);
	handler->init_tkn.width = ft_atoi(&line[8]);
	ft_strdel(&line);
	handler->init_tkn.area = handler->init_tkn.height
								* handler->init_tkn.width;
	handler->init_tkn.data =
		(char **)ft_memalloc(handler->init_tkn.height * sizeof(char *));
	i = 0;
	while (i < handler->init_tkn.height)
	{
		get_next_line(STDIN_FILENO, &temp);
		handler->init_tkn.data[i] = ft_strdup(temp);
		ft_strdel(&temp);
		i++;
	}
	cut_token(handler);
}

void		parse_filler_input(t_filler *handler)
{
	if (!(handler->me && handler->opp))
		get_players(handler);
	get_map(handler);
	get_token(handler);
}
