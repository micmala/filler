/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 18:59:26 by mmalanch          #+#    #+#             */
/*   Updated: 2018/09/26 18:59:27 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_CORE_H
# define FILLER_CORE_H

# include <stdbool.h>
# include <libft.h>
# include <ft_printf/ft_printf.h>
# include <get_next_line.h>
# include <filler_structs.h>

void			parse_filler_input(t_filler *handler);
void			cut_token(t_filler *handler);
bool			place_filler_token(t_filler *handler);
#endif
