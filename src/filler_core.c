/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 18:32:29 by mmalanch          #+#    #+#             */
/*   Updated: 2018/09/26 18:32:30 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <filler_core.h>

static inline int	manhattan_distance(t_point a, t_point b)
{
	return (FT_ABS(a.x - b.x) + FT_ABS(a.y - b.y));
}

static int			get_distance(t_filler *handler, t_point point)
{
	int		res;
	t_point	opp;

	res = handler->map.area;
	opp.y = 0;
	while (opp.y < handler->map.height)
	{
		opp.x = 0;
		while (opp.x < handler->map.width)
		{
			if (handler->map.data[opp.y][opp.x] == handler->opp)
				if (manhattan_distance(point, opp) < res)
					res = manhattan_distance(point, opp);
			opp.x++;
		}
		opp.y++;
	}
	return (res);
}

static bool			can_place_at_point(t_filler *flr, t_point pnt)
{
	int		count;
	t_point	i;

	if (pnt.y + flr->cut_tkn.height > flr->map.height ||
		pnt.x + flr->cut_tkn.width > flr->map.width)
		return (false);
	count = 0;
	i.y = 0;
	while (i.y < flr->cut_tkn.height)
	{
		i.x = -1;
		while (++i.x < flr->cut_tkn.width)
		{
			if (flr->cut_tkn.data[i.y][i.x] == '*')
			{
				if (flr->map.data[i.y + pnt.y][i.x + pnt.x] == flr->opp)
					return (false);
				count += (flr->map.data[i.y + pnt.y][i.x + pnt.x] == flr->me);
			}
			if (count > 1)
				return (false);
		}
		i.y++;
	}
	return (count == 1);
}

static t_point		get_token_point(t_filler *handler)
{
	t_point	point;
	t_point	res;
	int		min_dist;
	int		curr_dist;

	min_dist = handler->map.area;
	res.x = -1;
	res.y = -1;
	point.y = 0;
	while (point.y < handler->map.height)
	{
		point.x = 0;
		while (point.x < handler->map.width)
		{
			if (can_place_at_point(handler, point)
			&& (curr_dist = get_distance(handler, point)) < min_dist)
			{
				min_dist = curr_dist;
				res = point;
			}
			point.x++;
		}
		point.y++;
	}
	return (res);
}

bool				place_filler_token(t_filler *handler)
{
	t_point result_point;

	result_point = get_token_point(handler);
	if (result_point.x == -1 && result_point.y == -1)
	{
		ft_putstr("0 0\n");
		return (false);
	}
	ft_putnbr(result_point.y - handler->cut_diff.top);
	ft_putchar(' ');
	ft_putnbr(result_point.x - handler->cut_diff.left);
	ft_putchar('\n');
	return (true);
}
