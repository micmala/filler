/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmalanch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 19:00:05 by mmalanch          #+#    #+#             */
/*   Updated: 2018/09/26 19:00:06 by mmalanch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <filler_core.h>

static t_filler	*init_struct(void)
{
	t_filler *handler;

	handler = (t_filler *)malloc(sizeof(t_filler));
	handler->map.data = NULL;
	handler->map.width = -1;
	handler->map.height = -1;
	handler->map.area = 0;
	handler->init_tkn.data = NULL;
	handler->init_tkn.width = -1;
	handler->init_tkn.height = -1;
	handler->init_tkn.area = 0;
	handler->cut_tkn.data = NULL;
	handler->cut_tkn.width = -1;
	handler->cut_tkn.height = -1;
	handler->cut_tkn.area = 0;
	handler->me = 0;
	handler->opp = 0;
	return (handler);
}

static void		free_handler(t_filler *handler, bool ended)
{
	int i;

	i = 0;
	while (i < handler->map.height)
		ft_strdel(&(handler->map.data[i++]));
	free(handler->map.data);
	i = 0;
	while (i < handler->init_tkn.height)
		ft_strdel(&(handler->init_tkn.data[i++]));
	free(handler->init_tkn.data);
	i = 0;
	while (i < handler->cut_tkn.height)
		ft_strdel(&handler->cut_tkn.data[i++]);
	free(handler->cut_tkn.data);
	if (ended)
		free(handler);
}

int				main(void)
{
	t_filler *handler;

	handler = init_struct();
	while (true)
	{
		parse_filler_input(handler);
		if (!place_filler_token(handler))
		{
			free_handler(handler, true);
			return (1);
		}
		free_handler(handler, false);
	}
	return (0);
}
